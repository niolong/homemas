﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HM1
{
    class Program
    {
        static void Main(string[] args)
        {
            Random rnd = new Random();

            int n = 10;
            int[] mas;

            mas = new int[n];

            for(int i=0; i<n;i++)
            {
                mas[i] = rnd.Next(-10, 10 + 1);         
            }

            for(int i=0;i<n;i++)
            {
                if(mas[i]>=0)
                {
                    Console.Write(mas[i]+"  ");
                }
            }
            Console.WriteLine();
            for (int i = 0; i < n; i++)
            {
                if (mas[i] < 0)
                {
                    Console.Write(mas[i]+"  ");
                }
            }
            Console.ReadKey();
        }
    }
}
