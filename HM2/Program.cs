﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HM2
{
    class Program
    {
        static void Main(string[] args)
        {
            Random rnd = new Random();

            int n = 50;
            int[] mas = new int[n];

            for(int i=0; i<n;i++)
            {
                mas[i] = rnd.Next(1, 100);
            }

            for(int i = 0; i < n; i++)
            {
                if(i%10==0)
                {
                    Console.Write(mas[i]+ "  ");
                }
            }
            Console.ReadKey();

        }
    }
}
