﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HM3
{
    class Program
    {
        static void Main(string[] args)
        {
            Random rnd = new Random();

            int n = 10;
            int[] mas = new int[n];

            for(int i=0; i<n;i++)
            {
                mas[i] = rnd.Next(1, 20);

                Console.Write(mas[i]+"   ");

                if (mas[i] >= 10)
                {
                    mas[i]*=mas[i];
                }
            }
            Console.WriteLine();

            for (int i=0; i<n;i++)
            {
                Console.Write(mas[i] + "  ");
            }

            Console.ReadKey();
        }
    }
}
