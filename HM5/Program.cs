﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HM5
{
    class Program
    {
        static void Main(string[] args)
        {
            Random rnd = new Random();

            int n = 22;
            int[] mas = new int[n];
            int count = 0;

            for(int i=0; i<n;i++)
            {
                mas[i] = rnd.Next(160, 200);
            }

            for (int i = 0; i < n; i++)
            {
                Console.Write(mas[i]+"  ");
            }

            Console.WriteLine();

            for (int i = 0; i < n; i++)
            {
                if(mas[i]<180)
                {
                    count++;
                }
            }

            Console.Write(count);

            Console.ReadKey();
        }
    }
}
