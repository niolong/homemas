﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SpecMas
{
    class Program
    {
        static void Main(string[] args)
        {           
            int num1, num2;
            int count1=0, count2=0;
            int value1,value2;
            int sum = 0;
            int max;

            Console.WriteLine("Введите 1е число: ");
            num1 = int.Parse(Console.ReadLine());

            value1=num1;

            while (num1 > 0)
            {
                num1 = num1 / 10;
                count1++;
            }

            Console.WriteLine("Введите 2е число: ");
            num2 = int.Parse(Console.ReadLine());

            value2 = num2;

            while (num2 > 0)
            {                
                num2 = num2 / 10;
                count2++;
            }

            int[] masResult;

            if (count1>count2)
            {
                masResult = new int[count1];
                max = count1;
            }
            else
            {
                masResult = new int[count2];
                max = count2;
            }

            int[] mas = new int[count1];
            int[] mas2 = new int[count2];

              for(int i=count1-1; i>=0;i--)
              {
                  mas[i] = value1 % 10;
                  value1 = value1 / 10;                         
              }

              for (int i = count2-1; i >=0; i--)
              {
                  mas2[i] = value2 % 10;
                  value2 = value2 / 10;
              }

            if (count1>=count2)
            {
                for(int i=0; i<count1;i++)
                {
                    if (i<count2)
                    {
                        masResult[i] = mas[i] + mas2[i];
                    }
                    else
                    {
                        masResult[i] = mas[i] + 0;
                    }
                }

                for (int i = 0; i<count1; i++)
                {
                    Console.Write($"{masResult[i]} ");
                }
            }
            else
            {
                for (int i = 0; i < count2; i++)
                {
                    if (i < count1)
                    {
                        masResult[i] = mas[i] + mas2[i];
                    }
                    else
                    {
                        masResult[i] = mas2[i] + 0;
                    }
                }

                for (int i = 0; i < count2; i++)
                {
                    Console.Write($"{masResult[i]} ");
                }
            }

            for(int i=0; i<max;i++)
            {
                sum = masResult[i] + sum;
            }

            Console.WriteLine($"Общая сумма =  {sum}");

            Console.ReadKey();
        }
    }
}
