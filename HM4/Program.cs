﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HM4
{
    class Program
    {
        static void Main(string[] args)
        {
            Random rnd = new Random();

            int n = 10;
            int[] mas = new int[n];
            int sum = 0;

            for(int i=0; i<n; i++)
            {
                mas[i]= rnd.Next(1, 40);
            }

            for (int i = 0; i < n; i++)
            {
                Console.Write(mas[i]+"  ");
            }

            Console.WriteLine();

            for (int i=0;i<n;i++)
            {
               if(mas[i]<20)
                {
                    sum = sum + mas[i]; 
                }
            }
            Console.WriteLine(sum);

            Console.ReadKey();
        }
    }
}
